#!/usr/bin/python3

from csv import reader
from random import Random

words1 = dict()
words2 = dict()
r = Random()

db_words_count = 0
with open("words.csv", "r") as f:
    for row in reader(f):
        if len(row) < 2:
            continue

        words1[row[0]] = row[1]
        words2[row[1]] = row[0]

db_words_count = len(words1)

words_count = int(input('How many words do you want to memoraize: '))
print("Words count: {}".format(words_count))

correct_answers = 0

for i in range(0, words_count):
    word_idx = r.randint(0, db_words_count-1)
    given_translation = input("Give me translation of {}: "
            .format(list(words1.keys())[word_idx]))
    correct_translation = words1[list(words1.keys())[word_idx]]
    if given_translation == correct_translation:
        correct_answers += 1
        print("Correct answer")
    else:
        print("Bad answer. It should be: {}".format(correct_translation))


print("You has given {}/{} answers".format(correct_answers, words_count))
